
This is a first try at a java package.

It is an example for JNA (java native access) which binds the C method `printf` and runs it.

### Build
`./gradlew build`

### Run
`./gradlew run --args='some arguments'`

Output is produced by C's `printf`!
